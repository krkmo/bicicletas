var bici = require("../../models/bici");

//endpoints
//todas las lista
exports.bici_list = function (req, res) {
  res.status(200).json({
    bicicletas: bici.allBicis,
  });
};
// ingresar nueva bicicleta.
exports.bici_create = function (req, res) {
  var bicis = new bici(req.body.id, req.body.color, req.body.modelo);
  bicis.ubicacion = [req.body.lat, req.body.lng];

  bici.add(bicis);
  res.status(200).json({
    bicicleta: bicis,
  });
};

// eliminar bicicleta
exports.bici_delete = function (req, res) {
    bici.removeById(req.body.id);

    res.status(204).send("Eliminado");
  };


