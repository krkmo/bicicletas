var bici = require("../models/bici");

exports.bici_list = function (req, res) {
  res.render("bicicletas/index", { bicis: bici.allBicis });
};
exports.bici_create_get = function (req, res) {
  res.render("bicicletas/create");
};

exports.bici_create_post = function (req, res) {
  var bicis = new bici(req.body.id, req.body.color, req.body.modelo);
  bicis.ubicacion = [req.body.lat, req.body.lng];

  bici.add(bicis);

  res.redirect("/bicicletas");
};

exports.bici_update_get = function (req, res) {
  var bicis = bici.findById(req.params.id);
  res.render("bicicletas/update", { bicis });
};

exports.bici_update_post = function (req, res) {
  var bicis = bici.findById(req.params.id);
  bicis.id = req.body.id;
  bicis.color = req.body.color;
  bicis.modelo = req.body.modelo;
  bicis.ubicacion = [req.body.lat, req.body.lng];

  res.redirect("/bicicletas");
};

exports.bici_delete_post = function (req, res) {
  bici.removeById(req.body.id);
  res.redirect("/bicicletas");
};
