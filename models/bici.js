var bici = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
};

bici.prototype.toString = function () {
  return "id:" + this.id + " | color : " + this.color;
};

bici.allBicis = [];

bici.add = function (aBici) {
  bici.allBicis.push(aBici);
};

bici.findById = function (idBici) {
  var Id = bici.allBicis.find((x) => x.id == idBici);
  if (Id) return Id;
  else throw new Error(`No existe una bicicleta con es id ${idBici}`);
};

bici.removeById = function (idBici) {
  for (var i = 0; i < bici.allBicis.length; i++) {
    if (bici.allBicis[i].id == idBici) {
      bici.allBicis.splice(i, 1);
      break;
    }
  }
};

var a = new bici(1, "rojo", "urbana", [10.08493, -84.73154]);
var b = new bici(2, "blanca", "urbana", [10.08493, -84.7312184]);

bici.add(a);
bici.add(b);

module.exports = bici;
