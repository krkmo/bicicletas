var express = require('express');
var router = express.Router();

var biciController = require('../Controllers/biciController');

router.get('/',biciController.bici_list);
router.get('/create',biciController.bici_create_get);
router.post('/create',biciController.bici_create_post);
router.post('/:id/delete',biciController.bici_delete_post);

router.get('/:id/update',biciController.bici_update_get);
router.post('/:id/update',biciController.bici_update_post);

module.exports = router;