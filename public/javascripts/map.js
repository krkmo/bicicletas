var mymap = L.map("main_map").setView([10.08493, -84.73154], 18);
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(mymap);

/*L.marker([10.08493, -84.73154])
  .addTo(mymap)
  .bindPopup("Tienda de Bicicletas.")
  .openPopup();*/


$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result){
    console.log(result);

    result.bicicletas.forEach(function(bici){
      L.marker(bici.ubicacion,{title:bici.id})
        .addTo(mymap)
    });
  },
});
